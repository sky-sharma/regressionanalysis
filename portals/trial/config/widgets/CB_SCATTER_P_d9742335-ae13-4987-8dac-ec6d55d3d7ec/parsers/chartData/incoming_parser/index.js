/**
* The 'parser' variable is the entry point for your parser. Write logic inside of the provided function and return a value
* Constants and utility functions can be created outside of the parser
* The provided ctx parameter is an object that contains data and model information on this item
* @param {context} ctx 
* @returns {rtn} */
parser = (ctx) => {
  return [{"temperature":40,"humidity":50,"wind":14,"rainfall":5,"timestamp":"2016-06-22T02:26:14.368Z"},{"temperature":52,"humidity":60,"wind":8,"rainfall":6,"timestamp":"2016-06-23T02:27:41.112Z"},{"temperature":73,"humidity":70,"wind":4,"rainfall":0,"timestamp":"2016-06-24T02:27:51.132Z"}];
}