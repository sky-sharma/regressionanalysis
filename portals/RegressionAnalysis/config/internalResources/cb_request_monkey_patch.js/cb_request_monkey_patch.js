(function (opts) {
    console.log("opts", opts);
    let oldRequest = CB_PORTAL.ClearBlade.constructor.request;
    CB_PORTAL.ClearBlade.constructor.request = function (options, callback) {
        console.log('request', options, callback);
        oldRequest({ ...options, timeout: opts.timeout }, callback);
    }
})({ timeout: 300000 });
