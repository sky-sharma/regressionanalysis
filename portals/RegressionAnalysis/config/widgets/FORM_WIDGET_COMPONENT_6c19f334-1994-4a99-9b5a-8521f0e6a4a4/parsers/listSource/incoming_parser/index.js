
parser = (ctx) => {
  var productTypesParamsAddrsTimestamps = datasources.getProductTypesParamsAddrsTimestamps.incomingData.results;
  
  // Was getting errors when calling "calculateRegression" code-service with fields beginning with "@". So filtering them out.
  //var paramNames = paramNamesTimestamps.params.sort().filter((value, index, array) => {return(value[0] !== "@")});
  var productTypes = (Object.keys(productTypesParamsAddrsTimestamps.productTypesParamsAddrs)).sort();
  
  var dropDownOptions = productTypes.map(element => {
    var itemName = element;
    return { name: itemName, value: itemName }
  });
  
  var formSourceObj = {
    data: datasources.prodTypesFormData ? datasources.prodTypesFormData : {},
    overrideFieldSettings: {
      prodType: { label: 'Select a Product Type...', dropdownOptions: dropDownOptions },
    }
  };
  
  return formSourceObj
}
/*
var paramNames = paramNamesTimestamps.params.sort().filter((value, index, array) => {return(value[0] !== "@")});
  var dropDownOptions = paramNames.map(element => {
    var itemName = element;
    return {name: itemName, value: itemName}
  });
*/