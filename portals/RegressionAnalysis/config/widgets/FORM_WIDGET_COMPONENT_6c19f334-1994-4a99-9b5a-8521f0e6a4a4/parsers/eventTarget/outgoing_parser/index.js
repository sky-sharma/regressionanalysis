/**
* The 'parser' variable is the entry point for your parser. Write logic inside of the provided function and return a value
* Constants and utility functions can be created outside of the parser
* The provided ctx parameter is an object that contains data and model information on this item
* @param {context} ctx 
* @returns {rtn} */
parser = (ctx) => {
  CB_PORTAL.Loader.show();
  var prodType = ctx.widget.data.prodType;
  var productTypesParamsAddrsTimestamps = datasources.getProductTypesParamsAddrsTimestamps.incomingData.results;
  var productTypesParamsAddrs = productTypesParamsAddrsTimestamps.productTypesParamsAddrs[prodType];
  var params = productTypesParamsAddrs.parameters;
  var minAddr = productTypesParamsAddrs.minAddr;
  var maxAddr = productTypesParamsAddrs.maxAddr;
  var earliestTimestamp = productTypesParamsAddrsTimestamps.timestamps.earliestTimestamp;
  var latestTimestamp = productTypesParamsAddrsTimestamps.timestamps.latestTimestamp;
  var paramsAddrsTimestamps = {
    params: params,
    earliestTimestamp: earliestTimestamp,
    latestTimestamp: latestTimestamp,
    minAddr: minAddr,
    maxAddr: maxAddr
  };
  console.log("A");
  console.log(paramsAddrsTimestamps);
  return paramsAddrsTimestamps;
}