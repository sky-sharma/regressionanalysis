/**
* The 'parser' variable is the entry point for your parser. Write logic inside of the provided function and return a value
* Constants and utility functions can be created outside of the parser
* The provided ctx parameter is an object that contains data and model information on this item
* @param {context} ctx 
* @returns {rtn} */
parser = (ctx) => {
  var rawData = ctx.datasource;
  
  // Exit with a blank chart if this is the first time the datasource is being used (i.e. on startup)
  if (rawData === "placeholder") {
    return ([{'x': 0, 'Fit': 0}]);
  };

  var fitPlot = rawData.points.map((element) => {
        return ({'x': element[0], 'Fit': element[1]});
      });
  CB_PORTAL.Loader.hide();
  return fitPlot;
  }