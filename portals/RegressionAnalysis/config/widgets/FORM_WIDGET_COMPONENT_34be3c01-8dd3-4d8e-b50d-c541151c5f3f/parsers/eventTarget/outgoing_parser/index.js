/**
* The 'parser' variable is the entry point for your parser. Write logic inside of the provided function and return a value
* Constants and utility functions can be created outside of the parser
* The provided ctx parameter is an object that contains data and model information on this item
* @param {context} ctx 
* @returns {rtn} */
parser = (ctx) => {
  var x = ctx.widget.data.x;
  var regrFx = datasources.formData.incomingData.regrFx;
  console.log(regrFx);
  var equation = datasources.rawData.incomingData.equation;
  var a = equation[0];
  var b = equation[1];
  var y;
  switch(regrFx) {
    
    case "linear":
      y = (a * x) + b;
      break

    case "exponential":
      y = a * Math.exp(b * x);
      break
    
    case "logarithmic":
      y = a + (b * Math.log(x));
      break

    case "power":
      y = a * Math.pow(x, b);
      break

    case "polynomial":
      // Need to reverse coefficients because highest order coefficient is at 0 index and vice versa
      equation.reverse();
      console.log(equation);
      y = 0;
      for (var i = 0; i < equation.length; i++) {
        y += equation[i] * Math.pow(x, i);
      };
      break
  }
  
  return {"calc_y": y}
}