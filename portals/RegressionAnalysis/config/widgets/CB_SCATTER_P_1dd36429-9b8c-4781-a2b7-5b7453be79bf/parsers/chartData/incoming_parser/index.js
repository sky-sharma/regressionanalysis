/**
* The 'parser' variable is the entry point for your parser. Write logic inside of the provided function and return a value
* Constants and utility functions can be created outside of the parser
* The provided ctx parameter is an object that contains data and model information on this item
* @param {context} ctx 
* @returns {rtn} */
parser = (ctx) => {
  var incomingData = datasources.formData.incomingData;
  
  // Exit with a blank chart if this is the first time the datasource is being used (i.e. on startup)
  if (incomingData === "placeholder") {
    return ([{'x': 0, 'y': 0, 'Raw': 0}]);
  };

  var regrParams = {
    regrFx: incomingData.regrFx,
    polyOrder: incomingData.polyOrder,
    startTime: incomingData.startTime,
    stopTime: incomingData.stopTime,
    indepVar: incomingData.indepVar,
    depVar: incomingData.depVar,
    minAddr: incomingData.minAddr,
    maxAddr: incomingData.maxAddr
  };
  
  var regrResults = datasources.calculateRegression.sendData(regrParams);
  var resolvedRegrResults = regrResults.then((data) => {
    return data.results
    }).then((data) => {
      datasources.rawData.sendData(data);
      var rawPlot = data.dataset.map((element) => {
        return ({'x': element[0], 'y': element[1], 'Raw': element[1]});
      });
      return rawPlot;
  });
  return resolvedRegrResults;
}