
parser = (ctx) => {
  CB_PORTAL.Loader.hide();
  var paramsAddrsTimestamps = datasources.paramsAddrsTimestamps.incomingData;
  console.log("B");
  console.log(paramsAddrsTimestamps);
  // Was getting errors when calling "calculateRegression" code-service with fields beginning with "@". So filtering them out.
  
  // Get unique list of params - params returned from code-service may have duplicates
  var params = Array.from(new Set(paramsAddrsTimestamps.params)).sort().filter((value, index, array) => {return(value[0] !== "@")});
  var dropDownOptions = params.map(element => {
    var itemName = element;
    return {name: itemName, value: itemName}
  });

  var earliestTimestamp = paramsAddrsTimestamps.earliestTimestamp;
  var latestTimestamp = paramsAddrsTimestamps.latestTimestamp;
  
  var minAddr = paramsAddrsTimestamps.minAddr;
  var maxAddr = paramsAddrsTimestamps.maxAddr;

  var formSourceObj = {
    data: {
      minAddr: minAddr,
      maxAddr: maxAddr
    },
    overrideFieldSettings: {
      startTime: {label: "Select a Sample Start Time...", startDate: earliestTimestamp, endDate: latestTimestamp},
      stopTime: {label: "Select a Sample Stop Time...", startDate: earliestTimestamp, endDate: latestTimestamp},
      indepVar: {label: 'Select an Independent Variable (x)...', dropdownOptions: dropDownOptions},
      depVar: {label: 'Select a Dependent Variable (y)...', dropdownOptions: dropDownOptions}}
    };
  console.log("B2");
  console.log(formSourceObj);
  return formSourceObj
}