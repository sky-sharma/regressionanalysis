function findCoefficientsOfDetermination(req,resp){
    // These are parameters passed into the code service
    var params = req.params;
    ClearBlade.init({request: req});

    var typesParamsAddrs = productTypesParamsAddrs();
    
    // Just getting info. for Heat Pump Gen 5
    var hpwh_gen5 = typesParamsAddrs["Heat Pump Water Heater Gen 5"];
    var minAddr = hpwh_gen5.minAddr;
    var prodParams = hpwh_gen5.parameters;
    
    /*
    // Use history db to find some info.
    var historyDb = ClearBlade.Database({externalDBName: "mongo_history"});
    var callback = function(err, data) {
  		if(err) {
   		 	resp.error("Error performing external historyDb operation: " + JSON.stringify(data))
  		} else {
    		historyDbResults = data.Data;
        }
    }
    
    // Find earliest and latest timestamps
    var earliestDocCmd = 'db.history.find().sort({"_id": 1}).limit(1)';
    var latestDocCmd = 'db.history.find().sort({"_id": -1}).limit(1)';
    
    historyDb.performOperation(callback, earliestDocCmd);
    var earliestTimestamp = historyDbResults[0].timestamp;
    historyDb.performOperation(callback, latestDocCmd);
    var latestTimestamp = historyDbResults[0].timestamp;
    
    var objectIdFromDate = function (date) {
	    return Math.floor(date.getTime() / 1000).toString(16) + "0000000000000000";
    };

    var startTime = new Date(earliestTimestamp);
    var startObjectId = objectIdFromDate(startTime);

    var stopTime = new Date(latestTimestamp);
    var stopObjectId = objectIdFromDate(stopTime);
    */

    var historyDb = ClearBlade.Database({externalDBName: "mongo_history"});
    var cmdTemplateTypes = 'db.history.find({"$and":[{"{0}":{"$exists":true}},{"device_base_address":{"$eq":{1}}}]},{"_id": 0,"{2}": 1}).limit(1)';
        
    // Find all params whose types are float
    var floatParams = [];
    for (i = 0; i < prodParams.length; i++) {
        var paramToCheck = prodParams[i];
        var varSetCmd = cmdTemplateTypes.replace("{0}",paramToCheck).replace("{1}",minAddr).replace("{2}",paramToCheck);
        var varSetResults;
        var callback = function(err, data) {
            if(err) {
                log("Error: " + JSON.stringify(err) + "; Data: " + JSON.stringify(data));
                resp.error("Error performing external historyDb operation: " + JSON.stringify(data))
            } else {
                varSetResults = data.Data;
            }};

        historyDb.performOperation(callback, varSetCmd);
        //historyDb.performOperationAsync(callback, varSetCmd);
        var resultValObj = varSetResults[0];
        var resultValKey = Object.keys(resultValObj);
        var resultVal = resultValObj[resultValKey];

        // Check if value is a floating point number AND that it is not already in floatParams Array
        if (typeof(resultVal) !== "string" && !isNaN(resultVal) && !((resultVal % 1) === 0) &&
            (paramToCheck !== "UPELSIZE") && (paramToCheck !== "LOELSIZE") &&
            (floatParams.indexOf(paramToCheck) === -1))
            {
            floatParams.push(paramToCheck)
        };
    };
    log(floatParams);
    // Go through all params captured in previous section and compare each to the other as xy pairs.    
    var cmdTemplate = 'db.history.find({"$and":[{"{0}":{"$exists":true}},{"device_base_address":{"$eq":{1}}}]},{"_id": 0,"{2}": 1}).limit(10)';
    var paramsAndCoeffs = [];
    for (i = 0; i < floatParams.length; i++) {
        for (j = i + 1; j < floatParams.length; j++) {
            
            //if (i === j) continue;    // Only process when i and j are different.
            
            var xParam = floatParams[i];
            var yParam = floatParams[j];

            var xSetCmd = cmdTemplate.replace("{0}",xParam).replace("{1}",minAddr).replace("{2}",xParam);
            var ySetCmd = cmdTemplate.replace("{0}",yParam).replace("{1}",minAddr).replace("{2}",yParam);

            // Get array of x vals        
            var xSetResults;
            var callback = function(err, data) {
                if(err) {
                    log("Error: " + JSON.stringify(err) + "; Data: " + JSON.stringify(data));
                    resp.error("Error performing external historyDb operation: " + JSON.stringify(data))
                } else {
                    xSetResults = data.Data;
                }};
            historyDb.performOperation(callback, xSetCmd);
            //historyDb.performOperationAsync(callback, xSetCmd);
            
            // Get array of y vals
            var ySetResults;
            var callback = function(err, data) {
                if(err) {
                    log("Error: " + JSON.stringify(err) + "; Data: " + JSON.stringify(data));
                    resp.error("Error performing external historyDb operation: " + JSON.stringify(data))
                } else {
                    ySetResults = data.Data;
                }};
            historyDb.performOperation(callback, ySetCmd);
            //historyDb.performOperationAsync(callback, ySetCmd);

            // Get smaller array (x or y)
            var smallestSetSize = Math.min(xSetResults.length, ySetResults.length);
            
            var XY_DATASET = [];
            var YX_DATASET = [];    // Needed to reverse X and Y
            for (var i = 0; i < smallestSetSize; i++) {
                var XY_PAIR = [xSetResults[i][xParam], ySetResults[i][yParam]];
                XY_DATASET.push(XY_PAIR);
                YX_DATASET.push(XY_PAIR.reverse())
            };
            
            // Capture param pairs, calculate regression, calculate coefficients of determination
            var coeffOfDeterm;
            var paramsAndCoeff;

            const result_xy = regression.linear(XY_DATASET);
            coeffOfDeterm = result_xy.r2;
            if ((coeffOfDeterm !== null) && (!isNaN(coeffOfDeterm))) {
                paramsAndCoeff = [xParam, yParam, coeffOfDeterm];
                paramsAndCoeffs.push(paramsAndCoeff);
            };

            // Repeat with XY pairs reversed
            const result_yx = regression.linear(YX_DATASET);
            coeffOfDeterm = result_yx.r2;
            if ((coeffOfDeterm !== null) && (!isNaN(coeffOfDeterm))) {
                paramsAndCoeff = [yParam, xParam, coeffOfDeterm];
                paramsAndCoeffs.push(paramsAndCoeff);
            };
            if (j===10) resp.success(paramsAndCoeffs);
        };
    };
    
    /*
    if (regrFx === "polynomial") {
        const result = regression.polynomial(XY_DATASET, {order: polyOrder});
    } else {
        const result = eval("regression.{0}(XY_DATASET)".replace("{0}", (regrFx) ? regrFx : "linear"));
    }
    result.dataset = XY_DATASET;
    */
    resp.success(paramsAndCoeffs);
}
