function getParamNames(req, resp){
    req.userToken = doNotRead();
    ClearBlade.init({request: req});
    var historyDb = ClearBlade.Database({externalDBName: "mongo_history"});
    var paramNamesColl = ClearBlade.Collection({collectionName: "paramNames"});

    // Go through 10,000 records to find unique param names
    var historyDbCmd = "db.history.find().limit(1000)";
    
    var historyDbResults;
    var callback = function(err, data) {
  		if(err) {
   		 	resp.error("Error performing external historyDb operation: " + JSON.stringify(data))
  		} else {
    		historyDbResults = data.Data;
  		}}
	historyDb.performOperation(callback, historyDbCmd);
    var historyDbUniqueParams = [];
    
    for (var i = 0; i < historyDbResults.length; i++) {
        var param = Object.keys(historyDbResults[i])[0];
        if ((historyDbUniqueParams.indexOf(param) === -1) && (param!=="_id")) {
            historyDbUniqueParams.push(param);
            paramNamesColl.create({param_name: param}, function (err, data) {
                if (err) {
                    resp.error("Error in inserting paramName: " + JSON.stringify(data));
                }
            })
        };
    };

    var timestampsColl = ClearBlade.Collection({collectionName: "timestamps"});
    var earliestDocCmd = 'db.history.find().sort({"_id": 1}).limit(1)';
    var latestDocCmd = 'db.history.find().sort({"_id": -1}).limit(1)';
    
    historyDb.performOperation(callback, earliestDocCmd);
    var earliest_timestamp = historyDbResults[0].timestamp;
    historyDb.performOperation(callback, latestDocCmd);
    var latest_timestamp = historyDbResults[0].timestamp;
    timestampsColl.create({earliest: earliest_timestamp, latest: latest_timestamp}, function (err, data) {
                if (err) {
                    resp.error("Error in inserting paramName: " + JSON.stringify(data));
                }
            })
    resp.success(historyDbUniqueParams)
}