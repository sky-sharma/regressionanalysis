//XY_DATASET = [[0,1.7035290746392084],[1,4.13634742183881],[2,3.1945730501177225],[3,3.1492720687876217],[4,2.681366375270145],[5,1.0966986117393036],[6,3.6964214688907955],[7,3.1052507947217896],[8,2.2618002812182874],[9,4.591961850228523],[10,3.941547863761108],[11,5.19728941299109],[12,2.3496680828535492],[13,6.079534894792123],[14,4.867672520356505],[15,1.510866722894034],[16,5.7022077270557965],[17,6.668795681425197],[18,2.765384846962244],[19,3.459402953812954],[20,4.182254966889483],[21,4.26837691796552],[22,4.358906717213238],[23,6.128308190297125],[24,2.906558876848406],[25,2.6890053011080783],[26,6.230106639482443],[27,5.886970375744944],[28,5.617929264337617],[29,3.5446183704855216],[30,5.0816224161119115],[31,7.344414667560418],[32,4.4392189008074485],[33,6.634204104349175],[34,7.814592649011469],[35,6.219404209823827],[36,4.7151042192581745],[37,6.608728055139263],[38,6.488968109933459],[39,8.430310678290262]]
/**
 * Generates a linear reggression upon a 2D dataset
 * 
 * {@link http://tom-alexander.github.io/regression-js/ Utilizes the Linear Regression algorithm}
 *
 */
function calculateRegression(req, resp) {
    req.userToken = doNotRead();
    ClearBlade.init({request: req});

    // ANS 1
    var msg = ClearBlade.Messaging();

    var params = req.params;
    log(params);
    var regrFx = params.regrFx;
    var polyOrder = params.polyOrder;
    var startTimeStr = params.startTime;
    var stopTimeStr = params.stopTime;

    var x = params.indepVar;
    var y = params.depVar;

    var minAddr = params.minAddr;
    var maxAddr = params.maxAddr;


    // We need objectId because we are going to use it for the timestamp embedded within 4 of its bytes.
    // This is needed because the actual "timestamp" fields in the database are strings
    
    var objectIdFromDate = function (date) {
	    return Math.floor(date.getTime() / 1000).toString(16) + "0000000000000000";
    };

    var startTime = new Date(startTimeStr);
    var startObjectId = objectIdFromDate(startTime);

    var stopTime = new Date(stopTimeStr);
    var stopObjectId = objectIdFromDate(stopTime);

    var historyDb = ClearBlade.Database({externalDBName: "mongo_history"});

    var cmdTemplate = 'db.history.find({"$and":[{"{0}":{"$exists":true}},{"_id":{"$gte":{"$oid":"{1}"}}},{"_id":{"$lte":{"$oid":"{2}"}}},{"device_base_address":{"$eq":{3}}}]},{"_id": 0,"{4}": 1}).limit(60)';
    //var cmdTemplate = 'db.history.find({"$and":[{"{0}":{"$exists":true}},{"_id":{"$gte":{"$oid":"{1}"}}},{"_id":{"$lte":{"$oid":"{2}"}}}]},{"_id": 0,"{4}": 1}).limit(500)';
    var indepVarSetCmd = cmdTemplate.replace("{0}",x).replace("{1}", startObjectId).replace("{2}", stopObjectId).replace("{3}",minAddr).replace("{4}",x);
    var depVarSetCmd = cmdTemplate.replace("{0}",y).replace("{1}", startObjectId).replace("{2}", stopObjectId).replace("{3}",minAddr).replace("{4}",y);
    
    log(indepVarSetCmd);
    log(depVarSetCmd);
    
    //resp.success("success");

    var indepVarSetResults;
    var callback = function(err, data) {
  		if(err) {
            log("Error: " + JSON.stringify(err) + "; Data: " + JSON.stringify(data));
            msg.publish("error", JSON.stringify(data) + "; " + JSON.stringify(err));
   		 	// ANS 2 resp.error("Error performing external historyDb operation: " + JSON.stringify(data))
  		} else {
            msg.publish("success", "Independent Variable:");
            msg.publish("success", JSON.stringify(data));
    		indepVarSetResults = data.Data;
  		}};
	historyDb.performOperation(callback, indepVarSetCmd);

    var depVarSetResults;
    var callback = function(err, data) {
  		if(err) {
            log("Error: " + JSON.stringify(err) + "; Data: " + JSON.stringify(data));
            msg.publish("error", JSON.stringify(data) + "; " + JSON.stringify(err));
   		 	// ANS 2 resp.error("Error performing external historyDb operation: " + JSON.stringify(data))
  		} else {
            msg.publish("success", "Dependent Variable:");
            msg.publish("success", JSON.stringify(data));
    		depVarSetResults = data.Data;
  		}};
	historyDb.performOperation(callback, depVarSetCmd);

    var smallestSetSize = Math.min(indepVarSetResults.length, depVarSetResults.length);

    var XY_DATASET = [];
    for (var i = 0; i < smallestSetSize; i++) {
        var XY_PAIR = [indepVarSetResults[i][x], depVarSetResults[i][y]];
        XY_DATASET.push(XY_PAIR);
    };

    if (regrFx === "polynomial") {
        const result = regression.polynomial(XY_DATASET, {order: polyOrder});
    } else {
        const result = eval("regression.{0}(XY_DATASET)".replace("{0}", (regrFx) ? regrFx : "linear"));
    }
    result.dataset = XY_DATASET;
    log(result);
    msg.publish("success", "Best Fit:");
    msg.publish("success", JSON.stringify(result));
    resp.success(result);
}