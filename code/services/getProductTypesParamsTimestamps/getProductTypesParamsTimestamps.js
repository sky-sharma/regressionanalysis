/**
 * Type: Library
 * Description: A library that contains a function which, when called, returns an object with a public API.
 */
function getProductTypesParamsTimestamps(req, resp){
    
    // Need Dev credentials to access database in ver. 9.1.5 and older.
    req.userToken = doNotRead();
    ClearBlade.init({request: req});
    var historyDb = ClearBlade.Database({externalDBName: "mongo_history"});
    
    var productType = {
        TANKLESS_WATER_HEATER: "tanklessWaterHeater",
        HEAT_PUMP_WATER_HEATER_GEN4: "heatpumpWaterHeaterGen4",
        HEAT_PUMP_WATER_HEATER_GEN5: "heatpumpWaterHeaterGen5",
        HEAT_PUMP_WATER_HEATER_GEN1: "heatpumpWaterHeaterGen1",
        GAS_WATER_HEATER: "gasWaterHeater",
        ECONET_ZONE_CONTROLLER: "econetZoneController",
        ECONET_CONTROL_CENTER_GEN1: "econetControlCenterGen1",
        ECONET_CONTROL_CENTER: "econetControlCenter",
        ECONET_WIFI_TRANSLATOR: "econetWiFiTranslator",
        ELECTRIC_WATER_HEATER: "electricWaterHeater",
        TRITON_WATER_HEATER: "tritonWaterHeater",
        HOT_SPRING_WATER_HEATER: "hotspringWaterHeater",
        NIAGARA_WATER_HEATER: "niagaraWaterHeater",
        DRAGON_WATER_HEATER: "dragonWaterHeater",
        DRAGON_WATER_HEATER_C: "dragonWaterHeaterCommercial",
        HTPG_REFRIGERATOR_CONTROL: "htpgRefrigerationControl",
        HTPG_COMMAND_CENTERL: "htpgCommandCenter",
        CTA2045_UCM_AC_DEVICE: "CTA2045UCMACDevice",
        NOT_SUPPORTED_DEVICE: "deviceNotSupported"
    };

    var productTypesParamsAddrs = {
            "Heat Pump Water Heater":
            {
                deviceName: "Heat Pump Water Heater",
                deviceSubType: productType.HEAT_PUMP_WATER_HEATER_GEN1,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "WHTRENAB", "WHTRCNFG", "WHTRSETP", "VACA_NET", "HEATCTRL", "FAN_CTRL",
                "COMP_RLY", "HPWHSETP", "AMBIENTT", "LOHTRTMP", "UPHTRTMP", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04",
                "TOTALKWH", "WHTRCNFG", "WHTRSETP", "AMBIENTT", "LOHTRTMP", "UPHTRTMP", "POWRWATT"],
                minAddr: 320,
                maxAddr: 383
            },
            "Control Center": {
                deviceName: "Control Center",
                deviceSubType: productType.ECONET_CONTROL_CENTER_GEN1,
                //deviceType: HVAC_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "HEATSETP", "COOLSETP", "STATMODE", "STAT_FAN", "STATNFAN",
                "VACSTATE", "HVACMODE", "SPT", "RELH7005", "DEADBAND", "SPT_STAT", "HW_G_FAN", "FURNGFAN", "VACA_FAN",
                "DEADBAND", "SCHEDULE", "SCHEDOVR", "SCHEDULS", "SCHEDOVC", "SCHEDOVH", "SCHEDOVF", "TEMPOFFS", "DISPUNIT",
                "OAT_TEMP", "DHUMENAB", "DHUMSETP", "DHUM_OCL", "DH_DRAIN", "HUMDCNFG", "HUMDCTRL", "HUMDSETP", "HUMSMART",
                "AUTOTIME", "SMARTREC", "ALRMBEEP", "SCRNLOCK", "VACAENAB", "VACA_FAN", "COOLVACA", "HEATVACA", "VACASTIM",
                "VACAETIM", "VACASDAY", "VACAEDAY", "VACASMON", "VACAEMON", "VACASYOC", "VACAEYOC", "ALRMALRT", "ALARM_01",
                "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 512,
                maxAddr: 575
        },
        "Electric Water Heater": {
                deviceName: "Electric Water Heater",
                deviceSubType: productType.ELECTRIC_WATER_HEATER,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "DISPUNIT", "WHTRENAB", "WHTRCNFG", "WHTRSETP", "VACATION", "VACA_NET",
                "HEATCTRL", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 704,
                maxAddr: 767
        },
        "EcoNet WiFi Translator": {
                deviceName: "EcoNet WiFi Translator",
                deviceSubType: productType.ECONET_WIFI_TRANSLATOR,
                //deviceType: WIFI_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "WF_SSID", "WIFI_VER", "SW_VERSN", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03",
                "ALARM_04", "WFSIGNAL", "WFIPADDR", "timeZone", "DRBEVENT", "TOUSCHED", "UTILOVER", "SEASONMP", "SELSCHED", "WFSIGNAL",
                "SECCOUNT", "SCHEDULA", "SCHEDULB", "SCHEDULC", "SCHEDULD", "SCHEDULE"],
                minAddr: 832,
                maxAddr: 895
        },
        "Smart Thermostat": {
                deviceName: "Smart Thermostat",
                deviceSubType: productType.ECONET_CONTROL_CENTER,
                //deviceType: HVAC_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "HEATSETP", "COOLSETP", "STATMODE", "STAT_FAN", "STATNFAN", "VACSTATE",
                "HVACMODE", "AUTOMODE", "AWAYMODE", "SPT", "RELH7005", "SPT_STAT", "HW_G_FAN", "FURNGFAN", "VACA_FAN", "DEADBAND", "SCHEDULE",
                "SCHEDOVR", "SCHEDULS", "SCHEDOVC", "SCHEDOVH", "SCHEDOVF", "TEMPOFFS", "DISPUNIT", "CI_EMAIL", "CI__NAME", "CI_COMPN",
                "CI_PHONE", "EQP_NAME", "OAT_TEMP", "DHUMENAB", "DHUMSETP", "DHUM_OCL", "DH_DRAIN", "HUMDCNFG", "HUMDCTRL", "HUMSMART",
                "HUMDSETP", "AUTOTIME", "SMARTREC", "ALRMBEEP", "SCRNLOCK", "VACAENAB", "VACSTATE", "COOLVACA", "HEATVACA", "VACASTIM",
                "VACAETIM", "VACASDAY", "VACAEDAY", "VACASMON", "VACAEMON", "VACASYOC", "VACAEYOC", "ALRMALRT", "ALARM_01", "ALARM_02",
                "ALARM_03", "ALARM_04"],
                minAddr: 896,
                maxAddr: 959
        },
        "EcoNet Zone Controller": {
                deviceName: "EcoNet Zone Controller",
                deviceSubType: productType.ECONET_ZONE_CONTROLLER,
                //deviceType: HVAC_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "HEATSETP", "COOLSETP", "STATMODE", "STAT_FAN", "STATNFAN", "SPT", "RELH7005",
                "SPT_STAT", "DEADBAND", "SCHEDULE", "SCHEDOVR", "SCHEDULS", "SCHEDOVC", "SCHEDOVH", "SCHEDOVF", "DISPUNIT", "EQP_NAME", "ZONE_ALL",
                "ZONE_IDN", "SMARTREC", "SCRNLOCK", "ALRMBEEP", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 1664,
                maxAddr: 1668
        },
        "Gas Water Heater": {
                deviceName: "Gas Water Heater",
                deviceSubType: productType.GAS_WATER_HEATER,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "WF_SSID", "ENVYTEMP", "RESETDEV", "SW_VERSN", "WHTRSETP", "WHTRCNFG", "VACA_NET", "EMAXSETP",
                "EGASSTAT", "WFSIGNAL", "WFIPADDR", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 1088,
                maxAddr: 1151
        },
        "Triton Water Heater": {
                deviceName: "Triton Water Heater",
                deviceSubType: productType.TRITON_WATER_HEATER,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "MODLACCS", "HEATCALL", "TANKTEMP", "FLUETEMP", "INLTTEMP", "FLAMECUR", "FANSPEED", "BLOWRPWM",
                "S1_AIRFL", "SW_VERSN", "S2_INPRS", "S3_EXPRS", "S4_GLINE", "AUXFSENS", "SHUT1REQ", "MODEL_ID", "SHUTOPEN", "SHUTCLOS", "LEAKSENR",
                "CHE_FIGN", "CHEFFAIL", "CHE_HIOP", "CHE_BMIN", "GASVALVE", "SW_VERSN", "ALARMS", "ALERTS", "AND1POWR", "AND2POWR", "AND3POWR","ANODSTAT",
                "AND1STAT", "AND2STAT", "AND3STAT", "WHTRENAB", "WHTRCNFG", "WHTRSETP", "VACA_NET", "OCCUPIED", "SCHEDULS", "VACACMND", "DISPUNIT", "VLVSTATE",
                "CHE_BLRT", "C_HEALTH", "T_HEALTH", "CHE_SIGN", "LEAKSHUT", "SHUTSTAT", "HEALTHCB", "HEALTHTK", "FLAMETST", "SHUTCNFG", "CHE_GASU", "CHE_GALS",
                "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 4544,
                maxAddr: 4607
        },
        "Gladiator Water Heater": {
                deviceName: "Gladiator Water Heater",
                deviceSubType: productType.HOT_SPRING_WATER_HEATER,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "WHTRENAB", "WHTRCNFG", "WHTRSETP", "VACA_NET", "LSDETECT", "SHUTOVER", "SHUT1REQ", "SHUTOFFV",
                "SHUTOVRT", "SHUTOPEN", "SHUTCLOS", "HEATCTRL", "HOTWATER", "UPHTRTMP", "LOHTRTMP", "POWRWATT", "BOARDTMP", "TOTALKWH", "LEAKSENR", "ALRMALRT",
                "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 4608,
                maxAddr: 4671
        },
        "Heat Pump Water Heater Gen 4": {
                deviceName: "Heat Pump Water Heater Gen 4",
                deviceSubType: productType.HEAT_PUMP_WATER_HEATER_GEN4,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODSERN", "SW_VERSN", "UNITTYPE", "WHTRENAB", "WHTRCNFG", "WHTRSETP", "VACA_NET", "HEATCTRL", "FAN_CTRL", "COMP_RLY",
                "AMBIENTT", "LOHTRTMP", "UPHTRTMP", "POWRWATT", "TOTALKWH", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04", "TOTALKWH", "WHTRCNFG",
                "WHTRSETP", "AMBIENTT", "LOHTRTMP", "UPHTRTMP", "POWRWATT"],
                minAddr: 4096,
                maxAddr: 4159
        },
        "Heat Pump Water Heater Gen 5": {
                deviceName: "Heat Pump Water Heater Gen 5",
                deviceSubType: productType.HEAT_PUMP_WATER_HEATER_GEN5,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODSERN", "UNITTYPE", "PRODMODN", "SW_VERSN", "SERIAL_N", "UPELSIZE", "LOELSIZE", "LSDETECT", "WHTRENAB", "WHTRCNFG",
                "WHTRSETP", "VACA_NET", "HEATCTRL", "FAN_CTRL", "COMP_RLY", "AMBIENTT", "LOHTRTMP", "UPHTRTMP", "POWRWATT", "WHTRMODE", "EVAPTEMP", "SUCTIONT",
                "DISCTEMP", "I_RMSVAL", "EXACTUAL", "HOTWATER", "SHUTOFFV", "SECCOUNT", "TOTALKWH", "ANODEA2D", "ANODESTS", "SHUTOPEN", "SHUTCLOS", "HRSHIFAN",
                "HRSLOFAN", "HRSUPHTR", "HRS_COMP", "HRSLOHTR", "DRESOVER", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04", "TOTALKWH", "WHTRCNFG",
                "WHTRSETP", "AMBIENTT", "LOHTRTMP", "UPHTRTMP", "POWRWATT"],
                minAddr: 4736,
                maxAddr: 4786
        },
        "Tankless Water Heater": {
                deviceName: "Tankless Water Heater",
                deviceSubType: productType.TANKLESS_WATER_HEATER,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "WHTRSETP", "WHTRENAB", "WHTRMODE", "WHTRCNFG", "HTRS__ON", "MAN_HTRS", "INSTANCE", "WTR_USED",
                "WTR_BTUS", "COM_CHIP", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 4160,
                maxAddr: 4223
        },
        "Niagra Water Heater": {
                deviceName: "Niagra Water Heater",
                deviceSubType: productType.NIAGARA_WATER_HEATER,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SW_VERSN", "WHTRSETP", "WHTRENAB", "WHTRMODE", "VACA_NET", "WHTRCNFG", "RPUMPMOD", "RPUMPFEN", "RPMPINST",
                "RCIRPUMP", "WATRSAVE", "ALTITUDE", "SCRNLOCK", "DISPUNIT", "FREEZING", "HTRS__ON", "RECIRCIN", "SCHEDULS", "MAN_HTRS", "INSTANCE", "MWTRUSED",
                "MGASKBTU", "COM_CHIP", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 4224,
                maxAddr: 4244
        },
        "Dragon Water Heater": {
                deviceName: "Dragon Water Heater",
                deviceSubType: productType.DRAGON_WATER_HEATER,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "PRODSERN", "SW_VERSN", "SERIAL_N", "WHTRSETP", "WHTRENAB", "VLVSTATE", "SCHEDULS", "OCCUPIED", "VACA_NET",
                "SETPTMAX", "MDLGROUP", "RESDIFFM", "SHUTOPEN", "SHUTCLOS", "FANSPEED", "BLOWRPWM", "AND1POWR", "AND2POWR", "AND1CURR", "AND2CURR", "ALRMALRT",
                "HEALTHCB", "C_HEALTH", "HEALTHTK", "T_HEALTH", "CHE_SIGN", "INLTTEMP", "TANKTEMP", "CHE_FIGN", "CHEFFAIL", "SHUTCNFG", "LEAKSHUT", "RCIRCNFG",
                "RCIRODMD", "HEALTHCI", "POWANODE", "HEALTHTI", "SOV_INST", "FLOWSENS", "CHE_FCDV", "CHE_BPDV", "HOTWATER", "FLOW_GPM", "FLAMECUR", "CHE_GALS",
                "CHE_GASU", "INSTANCE", "CHE_BMIN", "ALARM_01", "ALARM_02", "ALARM_03"],
                minAddr: 4480,
                maxAddr: 4543
        },
        "Dragon Water Heater C": {
                deviceName: "Dragon Water Heater C",
                deviceSubType: productType.DRAGON_WATER_HEATER_C,
                //deviceType: WATER_HEATER_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "PRODSERN", "SW_VERSN", "SERIAL_N", "WHTRSETP", "WHTRENAB", "VLVSTATE", "SCHEDULS", "OCCUPIED", "VACA_NET",
                "SETPTMAX", "MDLGROUP", "RESDIFFM", "SHUTOPEN", "SHUTCLOS", "FANSPEED", "BLOWRPWM", "AND1POWR", "AND2POWR", "AND1CURR", "AND2CURR", "ALRMALRT",
                "HEALTHCB", "C_HEALTH", "HEALTHTK", "T_HEALTH", "CHE_SIGN", "INLTTEMP", "TANKTEMP", "CHE_FIGN", "CHEFFAIL", "SHUTCNFG", "LEAKSHUT", "RCIRCNFG",
                "RCIRODMD", "HEALTHCI", "POWANODE", "HEALTHTI", "SOV_INST", "FLOWSENS", "CHE_FCDV", "CHE_BPDV", "HOTWATER", "FLOW_GPM", "FLAMECUR", "CHE_GALS",
                "CHE_GASU", "INSTANCE", "CHE_BMIN", "ALARM_01", "ALARM_02", "ALARM_03"],
                minAddr: 4416,
                maxAddr: 4479
        },
        "HTPG Refrigeration Control": {
                deviceName: "HTPG Refrigeration Control",
                deviceSubType: productType.HTPG_REFRIGERATOR_CONTROL,
                //deviceType: HVAC_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "INSTANCE", "COOLTYPE", "PRODSERN", "SW_VERSN", "COMP_REQ", "SETPOINT", "DEFRCTRL", "EXACTUAL", "EXVSUPER",
                "EXVCSHSP", "DEFRTIMM", "DEFRNEXT", "COMPCYCL", "DEFRMAXT", "DEFRTTMP", "DEFPOVER", "DEFRCNTM", "CREQTIME", "DEFRMODG", "GRP_ENAB", "ALHISCLR",
                "COMPMODE", "DEFRMODE", "SPACETMP", "SPACEALL", "EVAPTEMP", "EVTEMP2T", "SUCTIONT", "SAT_SUCT", "PRESGSUC", "DRAINTMP", "AUX_TEMP", "DIGITAL1",
                "DIGITAL2", "DIGITAL3", "DEF1RELY", "DEF2RELY", "AUX1RELY", "AUX2RELY", "PWM_FANC", "PRODLOCA", "HTPGENAB", "COOLSETP", "HTPGENAB", "REFRIGER",
                "COILTYPE", "DRAINCFG", "AUXTEMPC", "EXVSTEPR", "EXV_SHSP", "SETPHYST", "EGLEADER", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 16384,
                maxAddr: 16448
        },
        "HTPG Command Center": {
                deviceName: "HTPG Command Center",
                deviceSubType: productType.HTPG_COMMAND_CENTERL,
                //deviceType: HVAC_TYPE,
                parameters: ["PRODDESC", "PRODMODN", "SETPOINT", "SETPHYST", "DEFRTTMP", "DEFPOVER", "DEFRCNTM", "CREQTIME", "HTPGENAB", "COILTYPE", "DRAINCFG",
                "AUXTEMPC", "EXVSTEPR", "EXV_SHSP", "ALRMALRT", "ALARM_01", "ALARM_02", "ALARM_03", "ALARM_04"],
                minAddr: 16512,
                maxAddr: 16575
        },
        "CTA2045 UCM-AC Device": {
                deviceName: "CTA2045 UCM-AC Device",
                deviceSubType: productType.CTA2045_UCM_AC_DEVICE,
                //deviceType: HVAC_TYPE,
                parameters: ["PRODDESC", "MODE", "SETPOINT", "MSG_SUPPORT_CODE", "QUERY_PAY_LENGTH_CODE", "LOADSHED", "CUSTOMER_OVERRIDE_CODE", "GRID_EMERGENCY",
                "OUTSIDE_COMM_CODE", "OP_STATE_CODE", "GET_INFO_CODE", "GET_COMMODITY_READ", "LINK_ACK_CODE", "LINK_NAK_CODE", "APP_ACK_CODE", "APP_NAK_CODE",
                "GET_COMMODITY_READ_REPLY_CODE", "GET_INFO_READ_REPLY_CODE"],
                minAddr: 16576,
                maxAddr: 16639
        },
        "Unsupported": {
                deviceName: "",
                deviceSubType: productType.NOT_SUPPORTED_DEVICE,
                parameters: ["PRODDESC", "PRODMODN"]
        }
    };

    var productTypes = Object.keys()

    // Find earliest and latest timestamps
    var earliestDocCmd = 'db.history.find().sort({"_id": 1}).limit(1)';
    var latestDocCmd = 'db.history.find().sort({"_id": -1}).limit(1)';
    
    historyDb.performOperation(callback, earliestDocCmd);
    var earliestTimestamp = historyDbResults[0].timestamp;
    historyDb.performOperation(callback, latestDocCmd);
    var latestTimestamp = historyDbResults[0].timestamp;
            
    resp.success(productTypesParamsAddrs);
}
